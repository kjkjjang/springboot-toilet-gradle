package j.k.kim.toilet;

import java.util.List;

/**
 * 	Rest 결과용 class
 * 	toilet 갯수 , 좌표 목록 , pageing 한 현재 페이지, 전체 페이지 목록을 결과로 준다
 */
public class ToiletResult {

	/**
	 * 	현재 목록 page 
	 * 	전체 페이지 중 몇번째에 해당되는지 알 수 있다.
	 */
	private int page;

	/**
	 * 	현재 페이지의 list count
	 */
	private int count;
	
	/**
	 * 	전체 페이지 수
	 */
	private int maxPage;
	
	/**
	 * 	목록이 없을 때 뿌려줄 메세지
	 */
	private String message;

	private List<Toilet> toiletList;

	public ToiletResult() {
	}
	
	public ToiletResult(List<Toilet> toiletList, int page, int maxPage) {
		this.page = page;
		this.maxPage = maxPage;
		this.toiletList = toiletList;
		if (toiletList.size() == 0) {
			this.count = 0;
			this.message = "존재하지 않습니다.";
		} else {
			this.count = toiletList.size();
			this.message = "list";
		}

	}
	
	
	public int getPage() {
		return page;
	}

	public String getMessage() {
		return message;
	}

	public List<Toilet> getToiletList() {
		return toiletList;
	}

	public int getMaxPage() {
		return maxPage;
	}

	public int getCount() {
		return count;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return "page[" + page + "]count[" + count + "]maxPage[" + maxPage + "]message[" + message + "]list[" + toiletList + "]";
	}
}
