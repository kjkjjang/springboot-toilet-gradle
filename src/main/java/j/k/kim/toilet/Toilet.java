package j.k.kim.toilet;

/**
 * 	화장실 정보 축약형
 * 	화장실 명, 위도, 경도 정보
 */
public class Toilet {

	private String toilet_name;
	private double latitude;
	private double longitude;
	
	public Toilet() {
		
	}
	public Toilet(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Toilet(String toilet_name, double latitude, double longitude) {
		this.toilet_name = toilet_name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getToilet_name() {
		return toilet_name;
	}

	public void setToilet_name(String toilet_name) {
		this.toilet_name = toilet_name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "name[" + toilet_name + "]lat[" + latitude + "]lon[" + longitude + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Toilet) {
			Toilet that = (Toilet) obj;
			return this.toilet_name.equals(that.toilet_name) &&
					this.latitude == that.latitude &&
					this.longitude == that.longitude;
		}
		return false;
	}
}
