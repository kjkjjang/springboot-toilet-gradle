package j.k.kim.toilet;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * 	화장실 관련 mybatis mapper
 */
public interface ToiletMapper {
	String paging = 
			" limit 30 offset #{page} * 30 ";
	
	String selectSql =
				  " SELECT * "
				+ " FROM (SELECT * FROM TOILET_DATA WHERE latitude is not null and longitude is not null ORDER BY latitude, longitude) " ;	

	String allMaxPage = 
				" SELECT COUNT(*) / 30 FROM (" + selectSql + ")";
	String selectInboundSqlWithParam = 
			 " SELECT * "
			+ " FROM TOILET_DATA "
			+ " WHERE latitude is not null and latitude >= #{minLatitude} and latitude <= #{maxLatitude} "
			+ " and longitude is not null and longitude >= #{minLongitude} and longitude <= #{maxLongitude} "
			+ " ORDER BY latitude, longitude" ;

	
	String maxPage = 
			" SELECT COUNT(*) / 30 FROM (" + selectInboundSqlWithParam + ")";

	String selectOutboundSqlWithParam = 
			 " SELECT * "
			+ " FROM TOILET_DATA "
			+ " WHERE latitude is not null and latitude >= #{minLatitude} and latitude <= #{maxLatitude} "
			+ " and longitude is not null and longitude <= #{minLongitude} and longitude >= #{maxLongitude} "
			+ " ORDER BY latitude, longitude" ;

	String outBountMaxPage = 
			" SELECT COUNT(*) / 30 FROM (" + selectOutboundSqlWithParam + ")";
	
	
	
	
	@Select("SELECT * FROM TOILET_DATA WHERE latitude = #{latitude} and longitude = #{longitude}")
	Toilet getItem(@Param("latitude") double latitude, @Param("longitude") double longitude);
	
	@Select("SELECT * FROM TOILET_DATA WHERE latitude = #{latitude} and longitude = #{longitude}")
	Toilet getItemFromVO(Toilet toilet);
	
	@Select(selectSql + paging)
	List<Toilet> getItemList(@Param("page") int page);

	@Select(allMaxPage)
	int getAllMaxPage();
	
	@Select(selectInboundSqlWithParam + paging)
	List<Toilet> getListInBoundArea(
			@Param("minLatitude")  double minLatitude,
			@Param("maxLatitude")  double maxLatitude,
			@Param("minLongitude") double minLongitude,
			@Param("maxLongitude") double maxLongitude,
			@Param("page") int page
			);

	@Select(maxPage)
	int getMaxPage(			
			@Param("minLatitude")  double minLatitude,
			@Param("maxLatitude")  double maxLatitude,
			@Param("minLongitude") double minLongitude,
			@Param("maxLongitude") double maxLongitude
			);
	
	@Select(selectInboundSqlWithParam + paging)
	List<Toilet> getListOutBoundArea(
			@Param("minLatitude")  double minLatitude,
			@Param("maxLatitude")  double maxLatitude,
			@Param("minLongitude") double minLongitude,
			@Param("maxLongitude") double maxLongitude,
			@Param("page") int page
			);

	@Select(outBountMaxPage)
	int getOutboundMaxPage(			
			@Param("minLatitude")  double minLatitude,
			@Param("maxLatitude")  double maxLatitude,
			@Param("minLongitude") double minLongitude,
			@Param("maxLongitude") double maxLongitude
			);
}
