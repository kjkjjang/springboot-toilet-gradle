package j.k.kim.toilet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import j.k.kim.polygon.PolygonFactory;
import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.PolygonException;
import j.k.kim.polygon.items.Rectangle;
import j.k.kim.polygon.items.RectangleWithBound;

/**
 * 화장실 관련 웹 Controller
 */
@RestController
@RequestMapping("/toiletApi")
public class ToiletController {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ToiletServiceImpl service;

	@RequestMapping("/testToilet")
	public Toilet testToilet(@ModelAttribute My2DPoint point) {
		return new Toilet("test", point.getX(), point.getY());
	}

	@RequestMapping("/testToiletList")
	public Toilet testToiletList(@ModelAttribute My2DPoint point) {
		point.getListPoint().stream().forEach(item -> logger.debug(item.toString()));
		return new Toilet("testToiletList", 2222, 2222);
	}

	@RequestMapping("/findAll")
	public ToiletResult findAll(@RequestParam(name = "page", defaultValue = "0") int page) {
		return service.getAll(page);
	}

	@RequestMapping("/findToiletsWithSize")
	public ToiletResult findToiletsInArea(@ModelAttribute My2DPoint centerPoint,
			@RequestParam(name = "latSize", defaultValue = "0") double latSize,
			@RequestParam(name = "lngSize", defaultValue = "0") double lngSize,
			@RequestParam(name = "page", defaultValue = "0") int page) {
		RectangleWithBound rect = new RectangleWithBound(centerPoint, latSize, lngSize);
		logger.info("rectangle Inbound[" + rect.isInBoundRectangle() + "]");
		if (rect.isInBoundRectangle()) {
			return service.getListInBoundArea(rect, page);
		} else {
			return service.getListOutBoundArea(rect, page);
		}
	}

	
	@RequestMapping("/findToiletsInArea")
	public ToiletResult findToiletsInArea(@ModelAttribute My2DPoint point,
			@RequestParam(name = "page", defaultValue = "0") int page) {
		Rectangle rectangle = PolygonFactory.getPolygon(point.getListPoint());

		return service.getListInBoundArea(rectangle, page);
	}

	/**
	 * 	사각형 조건에 부합하지 않는 오류 발생 시 처리
	 */
	@ExceptionHandler(PolygonException.class)
	public ToiletResult polygonConflict(PolygonException ex) {
		ToiletResult result = new ToiletResult();
		result.setMessage(ex.getErrorMessage());
		return result;
	}

	
}
