package j.k.kim.toilet;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.Rectangle;
import j.k.kim.polygon.items.RectangleWithBound;

/**
 * 	화장실 관련 서비스 
 */
@Service
public class ToiletServiceImpl {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ToiletMapper mapper;
	
	public Toilet getItem(double latitude, double longitude) {
		return mapper.getItem(latitude, longitude);
	}
	
	public Toilet getItemFromVo(Toilet toilet) {
		return mapper.getItemFromVO(toilet);
	}
	
	public ToiletResult getAll(int page) {
		List<Toilet> listResult = mapper.getItemList(page);
		int maxPage = mapper.getAllMaxPage();
		return new ToiletResult(listResult, page, maxPage);
	}

	public ToiletResult getListInBoundArea(Rectangle rectangle, int page) {
		return getListInBoundArea(rectangle.getMinX(), rectangle.getMaxX(), rectangle.getMinY(), rectangle.getMaxY(), page);
	}
	
	public ToiletResult getListInArea(List<My2DPoint> pointList, int page) {
		double minLatitude, maxLatitude;
		double minLongitude, maxLongitude;
		
		minLatitude = pointList.stream().map(My2DPoint::getX).min(Double::compare).get();
		maxLatitude = pointList.stream().map(My2DPoint::getX).max(Double::compare).get();
		minLongitude = pointList.stream().map(My2DPoint::getY).min(Double::compare).get();
		maxLongitude = pointList.stream().map(My2DPoint::getY).max(Double::compare).get();
		
		return getListInBoundArea(minLatitude, maxLatitude, minLongitude, maxLongitude, page);
	}

	private ToiletResult getListInBoundArea(double minX, double maxX, double minY, double maxY, int page) {
		List<Toilet> listResult = mapper.getListInBoundArea(minX, maxX, minY, maxY, page);
		
		logger.debug("listResult[" + listResult + "]");
		
		int maxPage = mapper.getMaxPage(minX, maxX, minY, maxY);
		return new ToiletResult(listResult, page, maxPage);
	}

	public ToiletResult getListOutBoundArea(RectangleWithBound rectangle, int page) {
		return getListOutBoundArea(rectangle.getMinX(), rectangle.getMaxX(), rectangle.getMinY(), rectangle.getMaxY(), page);
	}
	
	/**
	 * 	경도 관련 longitude 값으로 minY 보다 작은것 / maxY 보다 큰것 찾기
	 */
	private ToiletResult getListOutBoundArea(double minX, double maxX, double minY, double maxY, int page) {
		List<Toilet> listResult = mapper.getListOutBoundArea(minX, maxX, minY, maxY, page);
		
		logger.debug("listResult[" + listResult + "]");
		
		int maxPage = mapper.getOutboundMaxPage(minX, maxX, minY, maxY);
		return new ToiletResult(listResult, page, maxPage);
	}
	
}
