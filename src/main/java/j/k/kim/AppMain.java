package j.k.kim;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import j.k.kim.toilet.Toilet;
import j.k.kim.toilet.ToiletServiceImpl;

@SpringBootApplication
public class AppMain {

	@Autowired
	private ToiletServiceImpl service;
	
	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(AppMain.class);
		// TO-DO
		// context 초기화 후 xls 파일 파싱 하여 DB 넣기
		// 삼각형 안의 한점일 경우 1차원 직선 3개 로 상하 관계 판별하면 가능
		//  
	}
	
	@PostConstruct
	public void afterInit() {
		double latitude = 35.2637951152;
		double longitude = 128.4488311642;
		Toilet item = service.getItem(latitude, longitude);
		
		latitude = 35.263795115189694;
		longitude = 128.44883116420243	;
		item = service.getItem(latitude, longitude);
	}
}
