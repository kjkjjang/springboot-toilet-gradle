package j.k.kim.polygon;

import java.util.List;

import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.Rectangle;
import j.k.kim.polygon.items.Triangle;

/**
 * 	polygon factory
 */
public class PolygonFactory {

	@SuppressWarnings("unchecked")
	public static <T extends MyPolygon> T getPolygon(List<My2DPoint> points) {
		if (points == null || points.size() == 0) {
			throw new RuntimeException("Polygon 요건 해당 안됨");
		}
		
		if (points.size() == 1) {
			return (T) new My2DPoint(points.get(0));
		} else if (points.size() == 2) {
			return (T) new Rectangle(points.get(0), points.get(1)); // or line
		} else if (points.size() == 3){
			return (T) new Triangle(points);
		} else {
			throw new RuntimeException("거기까지는 힘들어~");
		}
	}
	
}
