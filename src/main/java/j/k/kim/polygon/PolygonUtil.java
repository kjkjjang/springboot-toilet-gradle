package j.k.kim.polygon;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import j.k.kim.polygon.items.Line;
import j.k.kim.polygon.items.My2DPoint;

/**	
 * 	수치 계산을 위한 static utility 클래스
 */
public class PolygonUtil {

	/*
	위도: 지표면의 어떤 지점에서 지표면에 대하여 수직선을 세우고, 이것과 적도면이 만나는 각도로 나타낸다.
	위도의 경우는 2πr × 1/360 이므로 2 × 3.14 × 6400km /360 = 114.64km
	즉 위도 1도간 거리는 114.64km이다.

	경도: 지구상의 한 지점을 지나는 자오선과 런던의 그리니치천문대를 지나는 본초자오선(本初子午線)의 각도이다.
	위도(X)에서 경도 1도의 길이는 2πr cos X × 1/360
	서울 부근(북위 38도에 가까움 정확히는 37도 30분)에서 경도 1도의 거리는 2 × 3.14 × 6400KM × 0.788 × 1/360 = 88km가 되는 것이다.
	즉 경도 1도간 거리는 88km

	실제 경도는
	1도 길이  88907.949 미터
	1분 길이   1481.799 미터
	1초 길이      24.697 미터

	실제 위도는
	1도 길이    110979.309 미터
	1분 길이      1849.655 미터
	1초 길이        30.828  미터
	*/
	
	/**
	 * 	두 좌표(위도,경도) 사이의 길이 
	 * @return m 단위 거리
	 */
	public static double calDistance(double lat1, double lon1, double lat2, double lon2) {

		double theta, dist;
		theta = lon1 - lon2;
		dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
				+ Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);

		dist = dist * 60 * 1.1515;
		dist = dist * 1.609344; // 단위 mile 에서 km 변환.
		dist = dist * 1000.0; // 단위 km 에서 m 로 변환

		return dist;
	}

	// 주어진 도(degree) 값을 라디언으로 변환
	public static double deg2rad(double deg) {
		return (double) (deg * Math.PI / (double) 180d);
	}

	// 주어진 라디언(radian) 값을 도(degree) 값으로 변환
	public static double rad2deg(double rad) {
		return (double) (rad * (double) 180d / Math.PI);
	}
	
	
	
	
	/**
	 * 	사각형과 한 점이 주어졌을 때
	 * 	한점이 포함되는 삼각형으로 분할
	 */
	public static List<List<My2DPoint>> rectangleToTriangleList(List<My2DPoint> rectangle, My2DPoint point) {
		assert(rectangle.size() == 4);
		
		List<List<My2DPoint>> rectangleToTriangleList = new ArrayList<>();
		
		for (int i = 0; i < rectangle.size(); i++) {
			List<My2DPoint> triangle = new ArrayList<>(rectangle);
			List<My2DPoint> removeList = new ArrayList<>();
			
			removeList.add(triangle.get(i));
			removeList.add(triangle.get( (i + 1) % triangle.size() ));
			
			triangle.removeAll(removeList);
			triangle.add(point);
			
			rectangleToTriangleList.add(triangle);
		}
		
		return rectangleToTriangleList.stream()
				.filter(triangleList -> !isLine(triangleList))
				.collect(Collectors.toList());
	}

	/**
	 * 	double 형 계산 편의를 위한 좌표 이동
	 * 	최소 x, 최소 y 를 기준으로 이동
	 */
	public static List<My2DPoint> moveTo(List<My2DPoint> list) {
		double minX, minY;
		
		minX = list.stream().map(My2DPoint::getX).min(Double::compare).get();
		minY = list.stream().map(My2DPoint::getY).min(Double::compare).get();

		return moveTo(list, minX, minY);
	}

	
	/**
	 * 	double 형 계산 편의를 위한 좌표 이동
	 * 	특정 x, 특정 y 를 기준으로 이동
	 * 	경도의 특성 상 좌 / 우 가 만나는 지점 처리 필요
	 */
	public static List<My2DPoint> moveTo(List<My2DPoint> list, double x, double y) {
		return list.stream().map(item -> {
			My2DPoint moved = new My2DPoint(item.getX() - x, item.getY() - y);
			return moved;
		}).collect(Collectors.toList());
	}
	
	/**
	 * 	좌표계 계산용
	 * @param list
	 * @param multi
	 * @return
	 */
	public static List<My2DPoint> geopointDouble(List<My2DPoint> list, double multi) {
		return list.stream().map(item -> {
			item.setX(item.getX() * multi);
			item.setY(item.getY() * multi);
			return item;
		}).collect(Collectors.toList());
	}
	
	public static List<My2DPoint> geopointDouble(List<My2DPoint> list) {
		return geopointDouble(list, 1000000);
	}	
	
	
	/**
	 * 	삼각형 구분용
	 * 	3점이 삼각형을 이루는지, 선분을 이루는지 확인 => 기울기 확인
	 */
	public static boolean isLine(List<My2DPoint> points) {
		assert(points.size() == 3);
		
		List<My2DPoint> temp = moveTo(points);
		
		My2DPoint p1 = temp.get(0);
		My2DPoint p2 = temp.get(1);
		My2DPoint p3 = temp.get(2);
		
		double dX1 = p2.getX() - p1.getX();
		double dY1 = p2.getY() - p1.getY();
		
		double dX2 = p3.getX() - p1.getX();
		double dY2 = p3.getY() - p1.getY();
		
		// divide by zero 해결용
		if (dX1 == 0 || dX2 == 0) {
			return dX1 == dX2 && dY1 == dY2;
		} else {
			return (dY1 / dX1) == (dY2 / dX2);
		}
	}

	/**
	 * 	직사각형 여부 확인
	 */
	public static boolean isRectangle(List<My2DPoint> rectangle) {
		if (!(rectangle.size() == 2 || rectangle.size() == 4)) {
			return false;
		}
		
		if (rectangle.size() == 4) {
			long distinctX = rectangle.stream().map(item -> item.getX()).distinct().count();
			long distinctY = rectangle.stream().map(item -> item.getY()).distinct().count();
			if (distinctX != 2 || distinctY != 2) {
				return false;
			}
		}
		
		if (rectangle.size() == 2) {
			long distinctX = rectangle.stream().map(item -> item.getX()).distinct().count();
			long distinctY = rectangle.stream().map(item -> item.getY()).distinct().count();
			if (distinctX != 2 || distinctY != 2) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 *	삼각형의 넓이 	
	 */
	public static double sizeOfTriangle(List<My2DPoint> triangle) {
		assert(triangle.size() == 3);
		
		List<My2DPoint> temp = moveTo(triangle);
		
		My2DPoint p1 = temp.get(0);
		My2DPoint p2 = temp.get(1);
		My2DPoint p3 = temp.get(2);
		
		
		return Math.abs( ((p1.getX() * p2.getY()) + (p2.getX() * p3.getY()) + (p3.getX() * p2.getY()) ) - 
				((p2.getX() * p1.getY()) + (p3.getX() * p2.getY()) + (p1.getX() * p3.getY()) ) ) / 2;
	}
	
	public static double getDistance(My2DPoint p1, My2DPoint p2) {
		List<My2DPoint> temp = new ArrayList<>();
		temp.add(p1);
		temp.add(p2);
		temp = moveTo(temp);
		
		My2DPoint moved1 = temp.get(0);
		My2DPoint moved2 = temp.get(1);
		
		return Math.sqrt(
					Math.pow(Math.abs(moved2.getX() - moved1.getX()), 2) + 
					Math.pow(Math.abs(moved2.getY() - moved1.getY()), 2)
				);
	}

	public static boolean isLineCollision(Line line1, Line line2) {
		return isLineCollision(line1.getPoints(), line2.getPoints());
	}

	
	/**
	 * 	라인 충돌 여부 확인 (교차점의 경우 충돌로 인정)
	 * 	http://eyshin05.tistory.com/entry/Line-Collision-Or-Intersect-Check-Algorithm
	 */
	public static boolean isLineCollision(List<My2DPoint> line1, List<My2DPoint> line2) {
		assert(line1.size() == 2);
		assert(line2.size() == 2);

		double minX, minY;
		
		minX = line1.stream().map(My2DPoint::getX).min(Double::compare).get();
		minY = line1.stream().map(My2DPoint::getY).min(Double::compare).get();

		List<My2DPoint> temp1 = moveTo(line1, minX, minY);
		List<My2DPoint> temp2 = moveTo(line2, minX, minY);
		
		
		My2DPoint p1 = temp1.get(0);
		My2DPoint p2 = temp1.get(1);
		
		My2DPoint p3 = temp2.get(0);
		My2DPoint p4 = temp2.get(1);

		double denominator = (p4.getY() - p3.getY()) * (p2.getX() - p1.getX()) 
								- (p4.getX() - p3.getX()) * (p2.getY() - p1.getY());
		
		// 평행 상태
		if (denominator == 0) {
			return false;
		}
		
		double numerator1 = ( (p4.getX() - p3.getX()) * (p1.getY() - p3.getY()) 
								- (p4.getY() - p3.getY()) * (p1.getX() - p3.getX())) / denominator;
		double numerator2 = ( (p2.getX() - p1.getX()) * (p1.getY() - p3.getY()) 
								- (p2.getY() - p1.getY()) * (p1.getX() - p3.getX())) / denominator;

		return (numerator1 >=0 && numerator1 <= 1) && (numerator2 >= 0 && numerator2 <= 1);
	}

}
