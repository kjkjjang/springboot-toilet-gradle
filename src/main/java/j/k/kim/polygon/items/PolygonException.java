package j.k.kim.polygon.items;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.OK)
public class PolygonException extends RuntimeException {

	private static final long serialVersionUID = -7701855097727586651L;

	private String errorMessage;

	public PolygonException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public PolygonException() {
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
