package j.k.kim.polygon.items;

import java.util.List;

import j.k.kim.polygon.MyPolygon;

/**
 * 2차원 점(좌표)
 */
public class My2DPoint implements MyPolygon {
	private double x;
	private double y;

	private List<My2DPoint> listPoint;

	public My2DPoint() {
	}

	public My2DPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public My2DPoint(My2DPoint point) {
		this.x = point.x;
		this.y = point.y;
	}

	public My2DPoint(List<My2DPoint> point) {
		this(point.get(0));
	}

	@Override
	public boolean isMyPolygon() {
		return true;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

	public List<My2DPoint> getListPoint() {
		return listPoint;
	}

	public void setListPoint(List<My2DPoint> listPoint) {
		this.listPoint = listPoint;
	}

	// Set 에 My2DPoint 넣을 때 중복 제거를 위한 hash / equals 메소드
	
	@Override
	public int hashCode() {
		return 0;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof My2DPoint) {
			My2DPoint that = (My2DPoint) obj;
			return this.x == that.x && this.y == that.y;
		}
		return false;
	}

}
