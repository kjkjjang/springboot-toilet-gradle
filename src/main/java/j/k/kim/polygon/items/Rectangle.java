package j.k.kim.polygon.items;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import j.k.kim.polygon.MyPolygon;
import j.k.kim.polygon.PolygonUtil;

/**
 * 	4개의 점으로 이루어진 사각형
 */
public class Rectangle implements MyPolygon {

	protected List<My2DPoint> points;

	public Rectangle() {
	}
	
	/**
	 * 	in side rectangle
	 */
	public Rectangle(List<My2DPoint> points) {
		this.points = (points);
		
		if (!isMyPolygon()) {
			throw new PolygonException("야야야야 이거 네모 아니야");
		}
		
	}
	
	/**
	 * 	in side rectangle
	 */
	public Rectangle(My2DPoint p1, My2DPoint p2) {
		// p1 left top / p2 right bottom  or switch case
		// or p1 right top / p2 left bottom  or switch case
		List<My2DPoint> list = new ArrayList<>();
		list.add(p1);
		list.add(p2);

		double minX = list.stream().map(My2DPoint::getX).min(Double::compare).get();
		double minY = list.stream().map(My2DPoint::getY).min(Double::compare).get();
		
		double maxX = list.stream().map(My2DPoint::getX).max(Double::compare).get();
		double maxY = list.stream().map(My2DPoint::getY).max(Double::compare).get();
		
		if (minX == maxX || minY == maxY) {
			throw new PolygonException("직사각형에 이런게 있나??");
		}
		
		My2DPoint p3, p4;

		if (	(p1.getX() == minX && p1.getY() == maxY) || 
				(p2.getX() == minX && p2.getY() == maxY) ) {
			// case of p1 left top , p2 right bottom or|| p2 left top , p1 right bottom
			// p3 left bottom / p4 right top
			p3 = new My2DPoint(minX, minY);
			p4 = new My2DPoint(maxX, maxY);
		} else if ( 
				(p1.getX() == maxX && p1.getY() == maxY ) ||
				(p2.getX() == maxX && p2.getY() == maxY ) ) {
			// case of p1 right top , p2 left bottom or|| p2 right top , p2 left bottom
			// p3 right bottom / p4 left top
			p3 = new My2DPoint(maxX, minY);
			p4 = new My2DPoint(minX, maxY);
		} else {
			throw new PolygonException("직사각형에 이런게 있나??");
		}

		this.points = new ArrayList<>();
		points.add(p1);
		points.add(p3);
		points.add(p2);
		points.add(p4);
		
	}

	/**
	 * 사각형 여부 확인 두 라인이 교차하는지 체크 // 0,0 -> 9,1 -> 10,10 -> 10.0 a, b, c, d 점에서 
	 * ab 기준 cd 
	 * bc 기준 da
	 */
	@Override
	public boolean isMyPolygon() {
		assert(points.size() == 4);
		
		List<My2DPoint> line1, line2;
		for (int i = 0; i < points.size() / 2; i++) {
			line1 = new ArrayList<>();
			line1.add(points.get(i));
			line1.add(points.get((i + 1) % points.size()));

			line2 = new ArrayList<>();
			line2.add(points.get((i + 2) % points.size()));
			line2.add(points.get((i + 3) % points.size()));

			
			if (PolygonUtil.isLineCollision(line1, line2)) {
				return false;
			}
		}
		return true;
	}

	public boolean containsPoint(My2DPoint point) {
		double minX = points.stream().map(My2DPoint::getX).min(Double::compare).get();
		double minY = points.stream().map(My2DPoint::getY).min(Double::compare).get();
		
		double maxX = points.stream().map(My2DPoint::getX).max(Double::compare).get();
		double maxY = points.stream().map(My2DPoint::getY).max(Double::compare).get();
		
		return point.getX() >= minX && point.getX() <= maxX &&
				point.getY() >= minY && point.getY() <= maxY;
	}
	
	public List<My2DPoint> getPoints() {
		return points;
	}

	@Override
	public String toString() {
		return points.stream().map(My2DPoint::toString).collect(Collectors.joining(","));
	}
	
	public double getMinX() {
		return points.stream().map(My2DPoint::getX).min(Double::compare).get();
	}
	public double getMaxX() {
		return points.stream().map(My2DPoint::getX).max(Double::compare).get();
	}
	public double getMinY() {
		return points.stream().map(My2DPoint::getY).min(Double::compare).get();
	}
	public double getMaxY() {
		return points.stream().map(My2DPoint::getY).max(Double::compare).get();
	}
	
}
