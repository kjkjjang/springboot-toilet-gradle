package j.k.kim.polygon.items;

import java.util.List;
import java.util.stream.Collectors;

import j.k.kim.polygon.MyPolygon;

/**
 * 	2개의 점으로 이루어진 선분	
 */
public class Line implements MyPolygon {

	private List<My2DPoint> points;
	
	public Line(List<My2DPoint> points) {
		this.points = points;
		
		if (!isMyPolygon()) {
			throw new RuntimeException("선분이 아니네~!");
		}
	}
	
	@Override
	public boolean isMyPolygon() {
		assert(getPoints().size() == 2);
		return true;
	}
	
	@Override
	public String toString() {
		return getPoints().stream().map(item -> item.toString()).collect(Collectors.joining(","));
	}

	public List<My2DPoint> getPoints() {
		return points;
	}

}
