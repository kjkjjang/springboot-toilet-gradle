package j.k.kim.polygon.items;

import java.util.ArrayList;

/**
 * 	중심점과 가로, 세로 길이로 이루어진 사각형
 * 	x => width  (latitde)
 * 	y => height (longitude)
 */
public class RectangleWithBound extends Rectangle {

	private My2DPoint point;
	
	private double x;
	private double y;
	
	public RectangleWithBound(My2DPoint centerPoint, double x, double y) {
		this.point = centerPoint;
		this.x = x;
		this.y = y;
		
		if (!isMyPolygon()) {
			throw new PolygonException("사각형의 조건이 안됨");
		}
		
		points = new ArrayList<>();
		points.add(new My2DPoint(centerPoint.getX() - x, centerPoint.getY() - y));
		points.add(new My2DPoint(centerPoint.getX() - x, centerPoint.getY() + y));
		points.add(new My2DPoint(centerPoint.getX() + x, centerPoint.getY() + y));
		points.add(new My2DPoint(centerPoint.getX() + x, centerPoint.getY() - y));
		
	}
	
	/**
	 * 	시간 경계선 근처 문제 해결용
	 * 	시간 경계선을 포함하는 사각형의 경우 열린 사각형의 모양 (out bound rectangle)
	 * 	시간 경계선을 포함하지 않는 사각형의 경우 닫힌 사각형의 모양 (in bound rectangle)
	 */
	public boolean isInBoundRectangle() {
		if (point.getY() + Math.abs(y) >= 180 || point.getY() - Math.abs(y) <= -180) {
			return false;
		} else {
			return true;
		}
	}
	
	@Override
	public boolean isMyPolygon() {
		if (x == 0 || y == 0) {
			return false;	
		} else {
			return true;
		}
		
	}

}
