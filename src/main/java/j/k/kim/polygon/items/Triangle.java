package j.k.kim.polygon.items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import j.k.kim.polygon.MyPolygon;

/**
 * 	3개의 점으로 이루어진 삼각형
 */
public class Triangle implements MyPolygon {

	private List<My2DPoint> points; 
	
	public Triangle(List<My2DPoint> points) {
		this.points = points;
		
		if (!isMyPolygon()) {
			throw new PolygonException("이것은 삼각형이 아닌 선분이여");
		}
	}
	
	public Triangle(My2DPoint p1, My2DPoint p2, My2DPoint p3) {
		points = new ArrayList<>();
		points.add(p1);
		points.add(p2);
		points.add(p3);
		
		if (!isMyPolygon()) {
			throw new PolygonException("이것은 삼각형이 아니다 붕꼐");
		}
	}

	/**
	 * 	점 3개가 선분에 해당되는지 확인
	 */
	@Override
	public boolean isMyPolygon() {
		assert(points.size() == 3);
		Set<My2DPoint> pointSet = new HashSet<>();
		pointSet.addAll(points);
		return pointSet.size() == 3;
	}
	
	/**
	 * 	사선식
	 * 		| ((x1 * y2) + (x2 * y3) + (x3 * y2) ) - ( (x2 * y1) + (x3 * y2) + (x1 * y3) ) | / 2 
	 * @return
	 */
	public double getArea() {
		My2DPoint p1 = points.get(0);
		My2DPoint p2 = points.get(1);
		My2DPoint p3 = points.get(2);
		return Math.abs( ((p1.getX() * p2.getY()) + (p2.getX() * p3.getY()) + (p3.getX() * p2.getY()) ) - 
							((p2.getX() * p1.getY()) + (p3.getX() * p2.getY()) + (p1.getX() * p3.getY()) ) ) / 2;
	}
	
	public boolean isInside(My2DPoint p) {
		int pnpoly = cn_PnPoly(p, this.points);
		boolean ret = pnpoly==0 ? false : true;
		return ret;
	}
	
	private int cn_PnPoly(My2DPoint P, List<My2DPoint> triangleList) {
		int cn = 0; // the crossing number counter

		// loop through all edges of the polygon
		for (int i = 0; i < triangleList.size(); i++) { // edge from V[i] to V[i+1]
			int nextIndex = (i == (triangleList.size() - 1)) ? 0 : i + 1;
			if (((triangleList.get(i).getY() <= P.getY()) && (triangleList.get(nextIndex).getY() > P.getY())) // an upward crossing
					|| ((triangleList.get(i).getY() > P.getY()) && (triangleList.get(nextIndex).getY() <= P.getY()))) { // a downward crossing
				// compute the actual edge-ray intersect x-coordinate
				double vt = (double) (P.getY() - triangleList.get(i).getY()) / (triangleList.get(nextIndex).getY() - triangleList.get(i).getY());
				if (P.getX() < triangleList.get(i).getX() + vt * (triangleList.get(nextIndex).getX() - triangleList.get(i).getX())) // P.getX() < intersect
					++cn; // a valid crossing of y=P.getY() right of P.getX()
			}
		}
		return (cn & 1); // 0 if even (out), and 1 if odd (in)
	}

}
