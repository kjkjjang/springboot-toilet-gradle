CREATE TABLE IF NOT EXISTS TOILET_DATA
(
	toilet_name	VARCHAR(300) ,
	latitude		DECIMAL(18, 15),
	longitude		DECIMAL(18, 15)
)
AS SELECT 화장실명, 위도, 경도 FROM CSVREAD('toilet.csv'); 
