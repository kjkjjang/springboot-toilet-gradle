package j.k.kim.toilet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ToiletControllerTest {

	@LocalServerPort
	private int port;

	Logger logger;
	
	private RestTemplate restTemplate;

	@Before
	public void setUp() {
		logger = LoggerFactory.getLogger(getClass());
		restTemplate = new RestTemplate();
	}

	@Test
	public void testToilet() throws Exception {
		String url = "http://localhost:" + port + "/toiletApi/testToilet?x=10&y=30";

		Toilet sample = new Toilet("test", 10.0, 30.0);
		Toilet restResult = this.restTemplate.getForObject(url, Toilet.class);
		Assert.assertEquals("Toilet Sample data", sample, restResult);
	}
	
	@Test
	public void testToiletList() throws Exception {
		String url = "http://localhost:" + port + "/toiletApi/testToiletList?" 
					+ "listPoint[0].x=35.2637951152&listPoint[0].y=128.4488311642&listPoint[1].x=35.2639080565&listPoint[1].y=128.4487320804";
		Toilet sample = new Toilet("testToiletList", 2222, 2222);

		Toilet restResult = this.restTemplate.getForObject(url, Toilet.class);
		Assert.assertEquals("Toilet Sample data", sample, restResult);
	}

	@Test
	public void testFindAll() {
		String url = "http://localhost:" + port + "/toiletApi/findAll";
		ToiletResult restResult = this.restTemplate.getForObject(url, ToiletResult.class);
		Assert.assertTrue(restResult.getCount() > 0);
	}

	@Test
	public void testFindToiletsInAreaExist() {
		String url = "http://localhost:" + port + "/toiletApi/findToiletsInArea?"
					+ "listPoint[0].x=35.264598&listPoint[0].y=128.447659&listPoint[1].x=35.263126&listPoint[1].y=128.449805&page=0";
		ToiletResult restResult = this.restTemplate.getForObject(url, ToiletResult.class);
		Assert.assertTrue(restResult.getCount() > 0);
	}

	@Test
	public void testFindToiletsInAreaNonExist() {
		String url = "http://localhost:" + port + "/toiletApi/findToiletsInArea?"
					+ "listPoint[0].x=37.648616&listPoint[0].y=129.396479&listPoint[1].x=37.647783&listPoint[1].y=129.398239&page=0";
		ToiletResult restResult = this.restTemplate.getForObject(url, ToiletResult.class);
		Assert.assertTrue(restResult.getCount() == 0);
	}

	@Test
	public void testFindToiletsInAreaException() {
		String url = "http://localhost:" + port + "/toiletApi/findToiletsInArea?"
					+ "listPoint[0].x=37.648616&listPoint[0].y=129.396479&listPoint[1].x=37.647783&listPoint[1].y=129.398239&page=0&"
					+ "listPoint[2].x=37.648616&listPoint[2].y=129.396479";
		ToiletResult result = this.restTemplate.getForObject(url, ToiletResult.class);
		Assert.assertTrue(result.getCount() == 0);
	}

	@Test
	public void testfindToiletsWithSizeNone() {
		String url = "http://localhost:" + port + "/toiletApi/findToiletsWithSize?"
					+ "x=37.648616&y=-178.396479&latSize=10&lngSize=20";
		ToiletResult result = this.restTemplate.getForObject(url, ToiletResult.class);
		Assert.assertTrue(result.getCount() == 0);
	}

	@Test
	public void testfindToiletsWithSizeExist() {
		String url = "http://localhost:" + port + "/toiletApi/findToiletsWithSize?"
					+ "x=35.2637951152&y=128.4488311642&latSize=0.5&lngSize=0.5";
		ToiletResult result = this.restTemplate.getForObject(url, ToiletResult.class);
		logger.info("testfindToiletsWithSizeExist======================" + result.toString());
		Assert.assertTrue(result.getCount() > 0);
	}
	
	
}


//http://localhost:8080/toiletApi/testToilet?x=35.2637951152&y=128.4488311642
//http://localhost:8080/toiletApi/testToiletList?listPoint[0].x=35.2637951152&listPoint[0].y=128.4488311642&listPoint[1].x=35.2639080565&listPoint[1].y=128.4487320804
//http://localhost:8080/toiletApi/findToiletsInArea?listPoint[0].x=35.264598&listPoint[0].y=128.447659&listPoint[1].x=35.263126&listPoint[1].y=128.449805
//http://localhost:8080/toiletApi/findToiletsWithSize?x=37.648616&y=-178.396479&latSize=10&lngSize=20
//http://localhost:8080/toiletApi/findToiletsWithSize?x=35.2637951152&y=128.4488311642&latSize=0.5&lngSize=0.5
