package j.k.kim.toilet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import j.k.kim.AppMain;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {AppMain.class})
public class ToiletMapperTest {

	@Autowired
	private ToiletMapper mapper;
	
	@Test
	public void testMaxPage() {
		int maxPageCount = mapper.getAllMaxPage();
		Assert.assertTrue(maxPageCount > 0);
	}
	
}
