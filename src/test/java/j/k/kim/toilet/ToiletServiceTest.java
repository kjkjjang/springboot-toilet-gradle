package j.k.kim.toilet;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import j.k.kim.AppMain;
import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.RectangleWithBound;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {AppMain.class})
public class ToiletServiceTest {

	@Autowired
	private ToiletServiceImpl service;
	
	@Test
	public void testGetItem() {
		double latitude = 35.2637951152;
		double longitude = 128.4488311642;	
		Toilet item = service.getItem(latitude, longitude);
		Assert.assertEquals("입곡군립공원1(입구)", item.getToilet_name());
	}
	
	@Test
	public void testGetAllWithPage() {
		ToiletResult result = service.getAll(0);
		Assert.assertTrue(result != null && result.getCount() > 0);
	}

	@Test
	public void testGetListInBoundArea() {
		RectangleWithBound rectangle = new RectangleWithBound(new My2DPoint(35.2637951152, 128.4488311642), 0.1, 0.1);
		ToiletResult result = service.getListInBoundArea(rectangle, 0);
		
		Assert.assertTrue(result != null && result.getCount() > 0);
	}
	
	@Test
	public void testGetListOutBoundArea() {
		RectangleWithBound rectangle = new RectangleWithBound(new My2DPoint(35.2637951152, -128.4488311642), 2, 2);
		ToiletResult result = service.getListOutBoundArea(rectangle, 0);
		
		Assert.assertTrue(result != null && result.getCount() == 0);
	}
	
}
