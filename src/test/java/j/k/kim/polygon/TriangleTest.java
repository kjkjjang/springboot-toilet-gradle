package j.k.kim.polygon;

import org.junit.Assert;
import org.junit.Test;

import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.Triangle;

public class TriangleTest {

	@Test(expected = RuntimeException.class)
	public void testIsTriangleException() {

		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(0, 1);
		My2DPoint p3 = new My2DPoint(0, 0);

		new Triangle(p1, p2, p3);
	}
	
	@Test
	public void testIsTriangle() {

		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(0, 1);
		My2DPoint p3 = new My2DPoint(1, 0);

		Triangle triangle = new Triangle(p1, p2, p3);

		Assert.assertTrue(triangle.isMyPolygon());

	}

	@Test
	public void testGetArea() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 0);
		My2DPoint p3 = new My2DPoint(0, 2);

		Triangle triangle = new Triangle(p1, p2, p3);

		Assert.assertEquals("equals", 2.0, triangle.getArea(), 0.0);
	}

	@Test
	public void testIsPointInside() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 0);
		My2DPoint p3 = new My2DPoint(0, 2);

		Triangle triangle = new Triangle(p1, p2, p3);

		My2DPoint inSide = new My2DPoint(0, 1);
		
		Assert.assertEquals(true, triangle.isInside(inSide));
		
	}
	
	@Test
	public void testIsPointOutside() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 0);
		My2DPoint p3 = new My2DPoint(0, 2);

		Triangle triangle = new Triangle(p1, p2, p3);

		My2DPoint outSide = new My2DPoint(2, 1);
		
		Assert.assertEquals(false, triangle.isInside(outSide));
		
	}
}
