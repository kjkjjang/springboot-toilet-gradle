package j.k.kim.polygon;

import org.junit.Assert;
import org.junit.Test;

import j.k.kim.polygon.items.My2DPoint;
import j.k.kim.polygon.items.Rectangle;

public class RectangleTest {

	@Test(expected=RuntimeException.class)
	public void testIsNotRectangle() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(0, 2);
		new Rectangle(p1, p2);
	}

	@Test
	public void testIsRectangle() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 2);
		Rectangle rectangle = new Rectangle(p1, p2);
		
		Assert.assertEquals(true, rectangle.isMyPolygon());
	}

	@Test
	public void testPointInRectangle() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 2);
		Rectangle rectangle = new Rectangle(p1, p2);
		
		My2DPoint point = new My2DPoint(1, 1);
		
		Assert.assertEquals(true, rectangle.containsPoint(point));
	}

	@Test
	public void testPointOutRectangle() {
		My2DPoint p1 = new My2DPoint(0, 0);
		My2DPoint p2 = new My2DPoint(2, 2);
		Rectangle rectangle = new Rectangle(p1, p2);
		
		My2DPoint point = new My2DPoint(2, 3);
		
		Assert.assertEquals(false, rectangle.containsPoint(point));
		
	}

}
