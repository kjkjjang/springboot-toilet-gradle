# 전국 화장실 좌표데이터 조회

## 환경
- spring boot + mybatis + h2(memory db)

## 호출 방법 
* ToiletController 에 요청하여 json 형식으로 받는 rest api
* /toiletApi/findToiletsWithSize : 중앙 좌표 및 가로/세로 길이를 파라미터로 직사각형 내의 화장실을 조회 
  * page 파라미터는 필요시 추가 가능 (default = 0)
  * ex) http://kjk850.wendies.org/toiletApi/findToiletsWithSize?x=35.2637951152&y=128.4488311642&latSize=0.5&lngSize=0.5
  * ex) http://kjk850.wendies.org/toiletApi/findToiletsWithSize?x=35.2637951152&y=128.4488311642&latSize=0.5&lngSize=0.5&page=1
* /toiletApi/findToiletsInArea : 좌표 2개를 주어 직사각형 모양을 만들고 내부의 화장실을 조회 (deprecated - 경도 차이 180 내에서만 가능)
  * ex) http://kjk850.wendies.org/toiletApi/findToiletsInArea?listPoint[0].x=35.264598&listPoint[0].y=128.447659&listPoint[1].x=35.263126&listPoint[1].y=128.449805

## 결과 메세지
* page : 전체 페이지중 몇번째에 해당하는지 알 수 있다(0부터 시작)
* count : 현재 결과 리스트의 갯수를 알 수 있다
* maxPage : 전체 페이지의 수를 알 수 있다
* message : 특정상황 (오류 메세지)에 사용 
* toiletList : 화장실 목록의 데이터 (이름, 위도, 경도)
  * ex) http://kjk850.wendies.org/toiletApi/findToiletsWithSize?x=35.2637951152&y=128.4488311642&latSize=0.1&lngSize=0.1&page=0 
  * 결과는 {"page":0,"count":30,"maxPage":2,"message":"list","toiletList":[{"toilet_name":"거락숲 대량마을","latitude":35.166113,"longitude":128.370376},{"toilet_name":"옥방교","latitude":35.172769,"longitude":128.375375},{"toilet_name":"별천계곡","latitude":35.1822763092,"longitude":128.431135952},{"toilet_name":"별천계곡","latitude":35.1823085403,"longitude":128.4311540811},{"toilet_name":"옥수골 마을","latitude":35.1900485,"longitude":128.5232471},{"toilet_name":"광산사밑 계곡 (단계사 위)","latitude":35.1916043,"longitude":128.4929534},{"toilet_name":"신목 공용주차장","latitude":35.1919588,"longitude":128.4942326},{"toilet_name":"완월체육공원","latitude":35.196133,"longitude":128.547895},{"toilet_name":"수선정사 밑","latitude":35.196216,"longitude":128.547922},{"toilet_name":"신감교 앞","latitude":35.2011519,"longitude":128.5091536},{"toilet_name":"감천 정자나무옆","latitude":35.2015452,"longitude":128.5201074},{"toilet_name":"여항산등산로입구","latitude":35.2017974941,"longitude":128.424112871},{"toilet_name":"여항산등산로입구","latitude":35.201811653,"longitude":128.4241350788},{"toilet_name":"감천제일교회 앞","latitude":35.204029,"longitude":128.5102687},{"toilet_name":"성전암 등산로","latitude":35.207311,"longitude":128.351521},{"toilet_name":"여항면 공설운동장","latitude":35.2079746985,"longitude":128.434648323},{"toilet_name":"여항면 공설운동장","latitude":35.2079746985,"longitude":128.434648323},{"toilet_name":"일광기공 앞","latitude":35.2086655,"longitude":128.5158991},{"toilet_name":"감천계곡 입구","latitude":35.2086655,"longitude":128.5158991},{"toilet_name":"광려중학교앞 광려천변","latitude":35.2172782,"longitude":128.5042213},{"toilet_name":"삼계 파라솔앞 (교량밑)","latitude":35.2220184,"longitude":128.5045099},{"toilet_name":"두척계곡","latitude":35.2318771,"longitude":128.5382271},{"toilet_name":"안계초등학교 앞 공원","latitude":35.2322921,"longitude":128.4946342},{"toilet_name":"회성배수장 내 (송정마을 위)","latitude":35.2458685,"longitude":128.5445641},{"toilet_name":"현대A 택시승강장","latitude":35.2481477,"longitude":128.5246717},{"toilet_name":"함안역","latitude":35.2492972553,"longitude":128.424727846},{"toilet_name":"함안역","latitude":35.2492972553,"longitude":128.4247278463},{"toilet_name":"군북역","latitude":35.2520723768,"longitude":128.351901574},{"toilet_name":"군북역","latitude":35.2520723768,"longitude":128.3519015741},{"toilet_name":"무진정","latitude":35.2560589897,"longitude":128.423303536}]}
  